/**
 * Copyright (c) 2023 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium';
import router from '@ohos.router';
import CommonFunc from '../../../MainAbility/common/Common';
import { UiComponent, UiDriver, Component, Driver, UiWindow, ON, BY, MatchPattern, DisplayRotation, ResizeDirection,
  WindowMode, PointerMatrix, UiDirection, MouseButton } from '@ohos.UiTest';
export default function tabs_PadMarTest() {
  describe('Tabs_PadMarTest', function () {
    beforeEach(async function (done) {
      console.info("Tabs_PadMar beforeEach start");
      let options = {
        url: "MainAbility/pages/Tabs/Tabs_ParmsChange/Tabs_PadMar",
      }
      try {
        router.clear();
        let pages = router.getState();
        console.info("get Tabs_PadMar state pages:" + JSON.stringify(pages));
        if (!("Tabs_PadMar" == pages.name)) {
          console.info("get Tabs_PadMar pages.name:" + JSON.stringify(pages.name));
          let result = await router.push(options);
          await CommonFunc.sleep(2000);
          console.info("push Tabs_PadMar page result:" + JSON.stringify(result));
        }
      } catch (err) {
        console.error("push Tabs_PadMar page error " + JSON.stringify(err));
        expect().assertFail();
      }
      console.info("Tabs_PadMar beforeEach end");
      done();
    });
    afterEach(async function () {
      await CommonFunc.sleep(2000);
      globalThis.value.message.notify({name:'index', value:0});
      globalThis.value.message.notify({name:'padding', value:0});
      globalThis.value.message.notify({name:'margin', value:0});
      console.info("Tabs_PadMarTest after each called")
    });
    /**
     * @tc.number    SUB_ACE_TABS_PADMAR_TEST_0100
     * @tc.name      testTabsPad
     * @tc.desc      set padding of tabs to 20
     */
    it('testTabsPad', 0, async function (done) {
      console.info('[testTabsPad] START');
      globalThis.value.message.notify({name:'padding', value:20});
      await CommonFunc.sleep(1000);
      let strJson = getInspectorByKey('Tabs_PadMar_01');
      let obj = JSON.parse(strJson);
      expect(obj.$type).assertEqual('Tabs');
      expect(obj.$attrs.barPosition).assertEqual("BarPosition.Start");
      expect(obj.$attrs.index).assertEqual("0");
      expect(obj.$attrs.scrollable).assertEqual(true);
      expect(obj.$attrs.barMode).assertEqual('BarMode.Fixed');
      let Tabs_PadMar_01 = CommonFunc.getComponentRect('Tabs_PadMar_01');
      let Tabs_PadMar_001 = CommonFunc.getComponentRect('Tabs_PadMar_001');
      let Tabs_PadMar_011 = CommonFunc.getComponentRect('Tabs_PadMar_011');
      console.info(`[testTabsPad] type: ${JSON.stringify(obj.$type)}`);
      console.info(`[testTabsPad] barPosition: ${JSON.stringify(obj.$attrs.barPosition)}`);
      console.info(`[testTabsPad] index: ${JSON.stringify(obj.$attrs.index)}`);
      console.info(`[testTabsPad] scrollable: ${JSON.stringify(obj.$attrs.scrollable)}`);
      console.info(`[testTabsPad] vertical: ${JSON.stringify(obj.$attrs.vertical)}`);
      console.info(`[testTabsPad] barMode: ${JSON.stringify(obj.$attrs.barMode)}`);
      let driver = await Driver.create();
      await driver.swipe(Math.round(Tabs_PadMar_011.right - 30),
        Math.round(Tabs_PadMar_011.top + ((Tabs_PadMar_011.bottom - Tabs_PadMar_011.top) / 2)),
        Math.round(Tabs_PadMar_011.left + 30),
        Math.round(Tabs_PadMar_011.top + ((Tabs_PadMar_011.bottom - Tabs_PadMar_011.top) / 2)));
      await CommonFunc.sleep(1000);
      let Tabs_PadMar_002 = CommonFunc.getComponentRect('Tabs_PadMar_002');
      let Tabs_PadMar_012 = CommonFunc.getComponentRect('Tabs_PadMar_012');
      await driver.swipe(Math.round(Tabs_PadMar_011.right - 30),
        Math.round(Tabs_PadMar_012.top + ((Tabs_PadMar_012.bottom - Tabs_PadMar_012.top) / 2)),
        Math.round(Tabs_PadMar_012.left + 30),
        Math.round(Tabs_PadMar_012.top + ((Tabs_PadMar_012.bottom - Tabs_PadMar_012.top) / 2)));
      await CommonFunc.sleep(1000);
      let Tabs_PadMar_003 = CommonFunc.getComponentRect('Tabs_PadMar_003');
      let Tabs_PadMar_013 = CommonFunc.getComponentRect('Tabs_PadMar_013');
      let subGreen = CommonFunc.getComponentRect('Tabs_PadMar_Green');
      let subBlue = CommonFunc.getComponentRect('Tabs_PadMar_Blue');
      let subYellow = CommonFunc.getComponentRect('Tabs_PadMar_Yellow');

      console.info(`[testTabsPad] Tabs_PadMar_011.left -
      Tabs_PadMar_01.left = ${Tabs_PadMar_011.left - Tabs_PadMar_01.left}`);
      expect(Math.round(Tabs_PadMar_011.left - Tabs_PadMar_01.left)).assertEqual(vp2px(20));
      expect(Math.round(Tabs_PadMar_012.left - Tabs_PadMar_01.left)).assertEqual(vp2px(20));
      expect(Math.round(Tabs_PadMar_013.left - Tabs_PadMar_01.left)).assertEqual(vp2px(20));
      expect(Math.round(subGreen.left - Tabs_PadMar_01.left)).assertEqual(vp2px(20));
      expect(Math.round(subGreen.top - Tabs_PadMar_01.top)).assertEqual(vp2px(20));

      console.info(`[testTabsPad] Tabs_index_011.left = ${Tabs_PadMar_011.left}`);
      expect(Tabs_PadMar_011.left).assertEqual(Tabs_PadMar_001.left);
      expect(Tabs_PadMar_012.left).assertEqual(Tabs_PadMar_002.left);
      expect(Tabs_PadMar_013.left).assertEqual(Tabs_PadMar_003.left);
      expect(Tabs_PadMar_011.top).assertEqual(Tabs_PadMar_001.top);
      expect(Tabs_PadMar_012.top).assertEqual(Tabs_PadMar_002.top);
      expect(Tabs_PadMar_013.top).assertEqual(Tabs_PadMar_003.top);

      console.info(`[testTabsPad] Tabs_PadMar_011.bottom -
        Tabs_PadMar_011.top = ${Tabs_PadMar_011.bottom - Tabs_PadMar_011.top}`);
      expect(Math.round(Tabs_PadMar_011.bottom - Tabs_PadMar_011.top)).assertEqual(Math.round(vp2px(204)));
      expect(Math.round(Tabs_PadMar_012.bottom - Tabs_PadMar_012.top)).assertEqual(Math.round(vp2px(204)));
      expect(Math.round(Tabs_PadMar_013.bottom - Tabs_PadMar_013.top)).assertEqual(Math.round(vp2px(204)));
      expect(Math.round(Tabs_PadMar_011.right - Tabs_PadMar_011.left)).assertEqual(vp2px(290));
      expect(Math.round(Tabs_PadMar_012.right - Tabs_PadMar_012.left)).assertEqual(vp2px(290));
      expect(Math.round(Tabs_PadMar_013.right - Tabs_PadMar_013.left)).assertEqual(vp2px(290));

      console.info(`[testTabsPad] Tabs_PadMar_001.bottom -
        Tabs_PadMar_001.top = ${Tabs_PadMar_001.bottom - Tabs_PadMar_001.top}`);
      expect(Math.round(Tabs_PadMar_001.bottom - Tabs_PadMar_001.top)).assertEqual(Math.round(vp2px(204)));
      expect(Math.round(Tabs_PadMar_002.bottom - Tabs_PadMar_002.top)).assertEqual(Math.round(vp2px(204)));
      expect(Math.round(Tabs_PadMar_003.bottom - Tabs_PadMar_003.top)).assertEqual(Math.round(vp2px(204)));
      expect(Math.round(Tabs_PadMar_001.right - Tabs_PadMar_001.left)).assertEqual(vp2px(290));
      expect(Math.round(Tabs_PadMar_002.right - Tabs_PadMar_002.left)).assertEqual(vp2px(290));
      expect(Math.round(Tabs_PadMar_003.right - Tabs_PadMar_003.left)).assertEqual(vp2px(290));

      console.info(`[testTabsPad] subGreen.bottom = ${subGreen.bottom}`);
      expect(subGreen.bottom).assertEqual(Tabs_PadMar_001.top);
      expect(subGreen.bottom).assertEqual(subBlue.bottom);
      expect(subGreen.bottom).assertEqual(subYellow.bottom);

      console.info(`[testTabsPad]subGreen.bottom - subGreen.top = ${subGreen.bottom - subGreen.top}`);
      expect(Math.round(subGreen.bottom - subGreen.top)).assertEqual(Math.round(vp2px(56)));
      expect(Math.round(subBlue.bottom - subBlue.top)).assertEqual(Math.round(vp2px(56)));
      expect(Math.round(subYellow.bottom - subYellow.top)).assertEqual(Math.round(vp2px(56)));
      expect(Math.round(subGreen.right - subGreen.left)).assertEqual(Math.round(vp2px(290 / 3)));
      expect(Math.round(subBlue.right - subBlue.left)).assertEqual(Math.round(vp2px(290 / 3)));
      expect(Math.round(subYellow.right - subYellow.left)).assertEqual(Math.round(vp2px(290 / 3)));

      console.info('[testTabsPad] END');
      done();
    });
    /**
     * @tc.number    SUB_ACE_TABS_PADMAR_TEST_0200
     * @tc.name      testTabsMar
     * @tc.desc      set margin of tabs to 20
     */
    it('testTabsMar', 0, async function (done) {
      console.info('[testTabsMar] START');
      globalThis.value.message.notify({name:'margin', value:20});
      await CommonFunc.sleep(1000);
      let strJson = getInspectorByKey('Tabs_PadMar_01');
      let obj = JSON.parse(strJson);
      expect(obj.$type).assertEqual('Tabs');
      expect(obj.$attrs.barPosition).assertEqual("BarPosition.Start");
      expect(obj.$attrs.index).assertEqual("0");
      expect(obj.$attrs.scrollable).assertEqual(true);
      expect(obj.$attrs.barMode).assertEqual('BarMode.Fixed');
      let Tabs_PadMar_01 = CommonFunc.getComponentRect('Tabs_PadMar_01');
      let Tabs_PadMar_Column_01 = CommonFunc.getComponentRect('Tabs_PadMar_Column_01');
      let Tabs_PadMar_001 = CommonFunc.getComponentRect('Tabs_PadMar_001');
      let Tabs_PadMar_011 = CommonFunc.getComponentRect('Tabs_PadMar_011');
      console.info(`[testTabsMar] type: ${JSON.stringify(obj.$type)}`);
      console.info(`[testTabsMar] barPosition: ${JSON.stringify(obj.$attrs.barPosition)}`);
      console.info(`[testTabsMar] index: ${JSON.stringify(obj.$attrs.index)}`);
      console.info(`[testTabsMar] scrollable: ${JSON.stringify(obj.$attrs.scrollable)}`);
      console.info(`[testTabsMar] vertical: ${JSON.stringify(obj.$attrs.vertical)}`);
      console.info(`[testTabsMar] barMode: ${JSON.stringify(obj.$attrs.barMode)}`);
      let driver = await Driver.create();
      await driver.swipe(Math.round(Tabs_PadMar_011.right - 30),
        Math.round(Tabs_PadMar_011.top + ((Tabs_PadMar_011.bottom - Tabs_PadMar_011.top) / 2)),
        Math.round(Tabs_PadMar_011.left + 30),
        Math.round(Tabs_PadMar_011.top + ((Tabs_PadMar_011.bottom - Tabs_PadMar_011.top) / 2)));
      await CommonFunc.sleep(1000);
      let Tabs_PadMar_002 = CommonFunc.getComponentRect('Tabs_PadMar_002');
      let Tabs_PadMar_012 = CommonFunc.getComponentRect('Tabs_PadMar_012');
      await driver.swipe(Math.round(Tabs_PadMar_011.right - 30),
        Math.round(Tabs_PadMar_012.top + ((Tabs_PadMar_012.bottom - Tabs_PadMar_012.top) / 2)),
        Math.round(Tabs_PadMar_012.left + 30),
        Math.round(Tabs_PadMar_012.top + ((Tabs_PadMar_012.bottom - Tabs_PadMar_012.top) / 2)));
      await CommonFunc.sleep(1000);
      let Tabs_PadMar_003 = CommonFunc.getComponentRect('Tabs_PadMar_003');
      let Tabs_PadMar_013 = CommonFunc.getComponentRect('Tabs_PadMar_013');
      let subGreen = CommonFunc.getComponentRect('Tabs_PadMar_Green');
      let subBlue = CommonFunc.getComponentRect('Tabs_PadMar_Blue');
      let subYellow = CommonFunc.getComponentRect('Tabs_PadMar_Yellow');

      console.info(`[testTabsMar] Tabs_PadMar_01.left -
        Tabs_PadMar_Column_01.left = ${Tabs_PadMar_01.left - Tabs_PadMar_Column_01.left}`);
      expect(Math.round(Tabs_PadMar_01.left - Tabs_PadMar_Column_01.left)).assertEqual(vp2px(20));
      expect(Math.round(Tabs_PadMar_01.top - Tabs_PadMar_Column_01.top)).assertEqual(vp2px(20));

      console.info(`[testTabsMar] Tabs_index_011.left = ${Tabs_PadMar_011.left}`);
      expect(Tabs_PadMar_011.left).assertEqual(Tabs_PadMar_001.left);
      expect(Tabs_PadMar_012.left).assertEqual(Tabs_PadMar_002.left);
      expect(Tabs_PadMar_013.left).assertEqual(Tabs_PadMar_003.left);
      expect(Tabs_PadMar_011.top).assertEqual(Tabs_PadMar_001.top);
      expect(Tabs_PadMar_012.top).assertEqual(Tabs_PadMar_002.top);
      expect(Tabs_PadMar_013.top).assertEqual(Tabs_PadMar_003.top);

      console.info(`[testTabsMar] Tabs_PadMar_011.bottom -
        Tabs_PadMar_011.top = ${Tabs_PadMar_011.bottom - Tabs_PadMar_011.top}`);
      expect(Math.round(Tabs_PadMar_011.bottom - Tabs_PadMar_011.top)).assertEqual(Math.round(vp2px(244)));
      expect(Math.round(Tabs_PadMar_012.bottom - Tabs_PadMar_012.top)).assertEqual(Math.round(vp2px(244)));
      expect(Math.round(Tabs_PadMar_013.bottom - Tabs_PadMar_013.top)).assertEqual(Math.round(vp2px(244)));
      expect(Math.round(Tabs_PadMar_011.right - Tabs_PadMar_011.left)).assertEqual(vp2px(330));
      expect(Math.round(Tabs_PadMar_012.right - Tabs_PadMar_012.left)).assertEqual(vp2px(330));
      expect(Math.round(Tabs_PadMar_013.right - Tabs_PadMar_013.left)).assertEqual(vp2px(330));

      console.info(`[testTabsMar] Tabs_PadMar_001.bottom -
        Tabs_PadMar_001.top = ${Tabs_PadMar_001.bottom - Tabs_PadMar_001.top}`);
      expect(Math.round(Tabs_PadMar_001.bottom - Tabs_PadMar_001.top)).assertEqual(Math.round(vp2px(244)));
      expect(Math.round(Tabs_PadMar_002.bottom - Tabs_PadMar_002.top)).assertEqual(Math.round(vp2px(244)));
      expect(Math.round(Tabs_PadMar_003.bottom - Tabs_PadMar_003.top)).assertEqual(Math.round(vp2px(244)));
      expect(Math.round(Tabs_PadMar_001.right - Tabs_PadMar_001.left)).assertEqual(vp2px(330));
      expect(Math.round(Tabs_PadMar_002.right - Tabs_PadMar_002.left)).assertEqual(vp2px(330));
      expect(Math.round(Tabs_PadMar_003.right - Tabs_PadMar_003.left)).assertEqual(vp2px(330));

      console.info(`[testTabsMar] subGreen.bottom = ${subGreen.bottom}`);
      expect(subGreen.bottom).assertEqual(Tabs_PadMar_001.top);
      expect(subGreen.bottom).assertEqual(subBlue.bottom);
      expect(subGreen.bottom).assertEqual(subYellow.bottom);

      console.info(`[testTabsMar]subGreen.bottom - subGreen.top = ${subGreen.bottom - subGreen.top}`);
      expect(Math.round(subGreen.bottom - subGreen.top)).assertEqual(Math.round(vp2px(56)));
      expect(Math.round(subBlue.bottom - subBlue.top)).assertEqual(Math.round(vp2px(56)));
      expect(Math.round(subYellow.bottom - subYellow.top)).assertEqual(Math.round(vp2px(56)));
      expect(Math.round(subGreen.right - subGreen.left)).assertEqual(vp2px(110));
      expect(Math.round(subBlue.right - subBlue.left)).assertEqual(vp2px(110));
      expect(Math.round(subYellow.right - subYellow.left)).assertEqual(vp2px(110));
      console.info('[testTabsMar] END');
      done();
    });
    /**
     * @tc.number    SUB_ACE_TABS_PADMAR_TEST_0300
     * @tc.name      testTabsPadMar
     * @tc.desc      set padding of tabs to 20,margin of tabs to 20
     */
    it('testTabsPadMar', 0, async function (done) {
      console.info('[testTabsPadMar] START');
      globalThis.value.message.notify({name:'padding', value:20});
      globalThis.value.message.notify({name:'margin', value:20});
      await CommonFunc.sleep(1000);
      let strJson = getInspectorByKey('Tabs_PadMar_01');
      let obj = JSON.parse(strJson);
      expect(obj.$type).assertEqual('Tabs');
      expect(obj.$attrs.barPosition).assertEqual("BarPosition.Start");
      expect(obj.$attrs.index).assertEqual("0");
      expect(obj.$attrs.scrollable).assertEqual(true);
      expect(obj.$attrs.barMode).assertEqual('BarMode.Fixed');
      let Tabs_PadMar_01 = CommonFunc.getComponentRect('Tabs_PadMar_01');
      let Tabs_PadMar_Column_01 = CommonFunc.getComponentRect('Tabs_PadMar_Column_01');
      let Tabs_PadMar_001 = CommonFunc.getComponentRect('Tabs_PadMar_001');
      let Tabs_PadMar_011 = CommonFunc.getComponentRect('Tabs_PadMar_011');
      console.info(`[testTabsPadMar] type: ${JSON.stringify(obj.$type)}`);
      console.info(`[testTabsPadMar] barPosition: ${JSON.stringify(obj.$attrs.barPosition)}`);
      console.info(`[testTabsPadMar] index: ${JSON.stringify(obj.$attrs.index)}`);
      console.info(`[testTabsPadMar] scrollable: ${JSON.stringify(obj.$attrs.scrollable)}`);
      console.info(`[testTabsPadMar] vertical: ${JSON.stringify(obj.$attrs.vertical)}`);
      console.info(`[testTabsPadMar] barMode: ${JSON.stringify(obj.$attrs.barMode)}`);
      let driver = await Driver.create();
      await driver.swipe(Math.round(Tabs_PadMar_011.right - 30),
        Math.round(Tabs_PadMar_011.top + ((Tabs_PadMar_011.bottom - Tabs_PadMar_011.top) / 2)),
        Math.round(Tabs_PadMar_011.left + 30),
        Math.round(Tabs_PadMar_011.top + ((Tabs_PadMar_011.bottom - Tabs_PadMar_011.top) / 2)));
      await CommonFunc.sleep(1000);
      let Tabs_PadMar_002 = CommonFunc.getComponentRect('Tabs_PadMar_002');
      let Tabs_PadMar_012 = CommonFunc.getComponentRect('Tabs_PadMar_012');
      await driver.swipe(Math.round(Tabs_PadMar_011.right - 30),
        Math.round(Tabs_PadMar_012.top + ((Tabs_PadMar_012.bottom - Tabs_PadMar_012.top) / 2)),
        Math.round(Tabs_PadMar_012.left + 30),
        Math.round(Tabs_PadMar_012.top + ((Tabs_PadMar_012.bottom - Tabs_PadMar_012.top) / 2)));
      await CommonFunc.sleep(1000);
      let Tabs_PadMar_003 = CommonFunc.getComponentRect('Tabs_PadMar_003');
      let Tabs_PadMar_013 = CommonFunc.getComponentRect('Tabs_PadMar_013');
      let subGreen = CommonFunc.getComponentRect('Tabs_PadMar_Green');
      let subBlue = CommonFunc.getComponentRect('Tabs_PadMar_Blue');
      let subYellow = CommonFunc.getComponentRect('Tabs_PadMar_Yellow');

      console.info(`[testTabsPadMar] Tabs_PadMar_01.left -
        Tabs_PadMar_Column_01.left = ${Tabs_PadMar_01.left - Tabs_PadMar_Column_01.left}`);
      expect(Math.round(Tabs_PadMar_01.left - Tabs_PadMar_Column_01.left)).assertEqual(vp2px(20));
      expect(Math.round(Tabs_PadMar_01.top - Tabs_PadMar_Column_01.top)).assertEqual(vp2px(20));

      console.info(`[testTabsPadMar] Tabs_PadMar_011.left -
        Tabs_PadMar_01.left = ${Tabs_PadMar_011.left - Tabs_PadMar_01.left}`);
      expect(Math.round(Tabs_PadMar_011.left - Tabs_PadMar_01.left)).assertEqual(vp2px(20));
      expect(Math.round(Tabs_PadMar_012.left - Tabs_PadMar_01.left)).assertEqual(vp2px(20));
      expect(Math.round(Tabs_PadMar_013.left - Tabs_PadMar_01.left)).assertEqual(vp2px(20));
      expect(Math.round(subGreen.left - Tabs_PadMar_01.left)).assertEqual(vp2px(20));
      expect(Math.round(subGreen.top - Tabs_PadMar_01.top)).assertEqual(vp2px(20));

      console.info(`[testTabsPadMar] Tabs_index_011.left = ${Tabs_PadMar_011.left}`);
      expect(Tabs_PadMar_011.left).assertEqual(Tabs_PadMar_001.left);
      expect(Tabs_PadMar_012.left).assertEqual(Tabs_PadMar_002.left);
      expect(Tabs_PadMar_013.left).assertEqual(Tabs_PadMar_003.left);
      expect(Tabs_PadMar_011.top).assertEqual(Tabs_PadMar_001.top);
      expect(Tabs_PadMar_012.top).assertEqual(Tabs_PadMar_002.top);
      expect(Tabs_PadMar_013.top).assertEqual(Tabs_PadMar_003.top);

      console.info(`[testTabsPadMar] Tabs_PadMar_011.bottom -
        Tabs_PadMar_011.top = ${Tabs_PadMar_011.bottom - Tabs_PadMar_011.top}`);
      expect(Math.round(Tabs_PadMar_011.bottom - Tabs_PadMar_011.top)).assertEqual(Math.round(vp2px(204)));
      expect(Math.round(Tabs_PadMar_012.bottom - Tabs_PadMar_012.top)).assertEqual(Math.round(vp2px(204)));
      expect(Math.round(Tabs_PadMar_013.bottom - Tabs_PadMar_013.top)).assertEqual(Math.round(vp2px(204)));
      expect(Math.round(Tabs_PadMar_011.right - Tabs_PadMar_011.left)).assertEqual(vp2px(290));
      expect(Math.round(Tabs_PadMar_012.right - Tabs_PadMar_012.left)).assertEqual(vp2px(290));
      expect(Math.round(Tabs_PadMar_013.right - Tabs_PadMar_013.left)).assertEqual(vp2px(290));

      console.info(`[testTabsPadMar] Tabs_PadMar_001.bottom -
      Tabs_PadMar_001.top = ${Tabs_PadMar_001.bottom - Tabs_PadMar_001.top}`);
      expect(Math.round(Tabs_PadMar_001.bottom - Tabs_PadMar_001.top)).assertEqual(Math.round(vp2px(204)));
      expect(Math.round(Tabs_PadMar_002.bottom - Tabs_PadMar_002.top)).assertEqual(Math.round(vp2px(204)));
      expect(Math.round(Tabs_PadMar_003.bottom - Tabs_PadMar_003.top)).assertEqual(Math.round(vp2px(204)));
      expect(Math.round(Tabs_PadMar_001.right - Tabs_PadMar_001.left)).assertEqual(vp2px(290));
      expect(Math.round(Tabs_PadMar_002.right - Tabs_PadMar_002.left)).assertEqual(vp2px(290));
      expect(Math.round(Tabs_PadMar_003.right - Tabs_PadMar_003.left)).assertEqual(vp2px(290));

      console.info(`[testTabsPadMar] subGreen.bottom = ${subGreen.bottom}`);
      expect(subGreen.bottom).assertEqual(Tabs_PadMar_001.top);
      expect(subGreen.bottom).assertEqual(subBlue.bottom);
      expect(subGreen.bottom).assertEqual(subYellow.bottom);

      console.info(`[testTabsPadMar]subGreen.bottom - subGreen.top = ${subGreen.bottom - subGreen.top}`);
      expect(Math.round(subGreen.bottom - subGreen.top)).assertEqual(Math.round(vp2px(56)));
      expect(Math.round(subBlue.bottom - subBlue.top)).assertEqual(Math.round(vp2px(56)));
      expect(Math.round(subYellow.bottom - subYellow.top)).assertEqual(Math.round(vp2px(56)));
      expect(Math.round(subGreen.right - subGreen.left)).assertEqual(Math.round(vp2px(290 / 3)));
      expect(Math.round(subBlue.right - subBlue.left)).assertEqual(Math.round(vp2px(290 / 3)));
      expect(Math.round(subYellow.right - subYellow.left)).assertEqual(Math.round(vp2px(290 / 3)));

      console.info('[testTabsPadMar] END');
      done();
    });
  })
}
